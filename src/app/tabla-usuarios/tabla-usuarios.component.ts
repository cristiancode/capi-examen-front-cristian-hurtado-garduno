import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'boot-tabla-usuarios',
  templateUrl: './tabla-usuarios.component.html',
  styleUrls: ['./tabla-usuarios.component.css']
})
export class TablaUsuariosComponent {
  @Input() titulos: any;
  @Input() users: any;

  objectKeys = Object.keys;

  constructor() { }

  ngOnInit(): void {
  }
}
